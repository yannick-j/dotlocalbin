#!/bin/bash

# thème

color_good="#98971a"
color_degraded="#d65d0e"
color_bad="#cc241d"
color_bg2="#504945"
color_gray="#928374"
color_alert="#fb4934"

separator="^c$color_bg2^|^d^"

# clavier

# keyboard_layout=$(setxkbmap -query | awk '/layout/ {print $2}')
# keyboard_status="  $keyboard_layout $separator"
capslock_state=$(xset q | awk '/Caps Lock/ {print $4}')
if [ $capslock_state == "on" ]; then
    color=$color_alert
    # keyboard_status=" ^c$color^ $keyboard_layout^d^ $separator"
    keyboard_status=" ^c$color^ ^d^$separator"
fi

# connection ethernet

# color=$color_bad
# eth_status="^c$color^ ^d^"
eth_status=""

eth_ip=$(nmcli device show eth0 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [ -n "$eth_ip" ]; then
    color=$color_good
    eth_status=" ^c$color^ $eth_ip^d^ $separator"
    # eth_status="^c$color^ $eth_ip^d^"
    eth_speed=$(ethtool eth0 2>/dev/null | awk '/Speed/ {print $2}')

    case $eth_speed in
        1000Mb/s)
            color=$color_good
            eth_status=" ^c$color^ $eth_ip^d^ $separator";;
            # eth_status="^c$color^ $eth_ip 1Gb/s^d^";;
        100Mb/s)
            color=$color_degraded
            eth_status=" ^c$color^ $eth_ip^d^ $separator";;
            # eth_status="^c$color^ $eth_ip 100Mb/s^d^";;
        10Mb/s)
            color=$color_bad
            eth_status=" ^c$color^ $eth_ip^d^ $separator";;
            # eth_status="^c$color^ $eth_ip 10Mb/s^d^";;
        Unknown!)
            color=$color_bad
            # eth_speed="?Mb/s"
            eth_status=" ^c$color^ $eth_ip^d^ $separator";;
            # eth_status="^c$color^ $eth_ip $eth_speed^d^";;
    esac

fi

# connexions wifi

# color=$color_bad
# wifi_status="^c$color^^d^"
wifi_status=""

# connexion wlan0

wifi_ip=$(nmcli device show wlan0 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [ -n "$wifi_ip" ]; then
    wifi_ssid=$(nmcli connection show --active | awk '/wlan0/ {print $1}')
    wifi_link=$(cat /proc/net/wireless | awk 'NR==3 {print $3}' | head --bytes=-2)
    if [ $wifi_link -gt 33 ]; then
        color=$color_degraded
        if [ $wifi_link -gt 66 ]; then
            color=$color_good
        fi
    fi
    wifi_status=" ^c$color^ $wifi_link% $wifi_ssid $wifi_ip^d^ $separator"
fi

# connexion wlan1

wifi_ip=$(nmcli device show wlan1 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [ -n "$wifi_ip" ]; then
    wifi_ssid=$(nmcli connection show --active | awk '/wlan0/ {print $1}')
    wifi_link=$(cat /proc/net/wireless | awk 'NR==3 {print $3}' | head --bytes=-2)
    if [ $wifi_link -gt 33 ]; then
        color=$color_degraded
        if [ $wifi_link -gt 66 ]; then
            color=$color_good
        fi
    fi
    wifi_status=" ^c$color^ $wifi_link% $wifi_ssid $wifi_ip^d^ $separator"
fi

# connexion wlx086a0a976811

wlx_status=""
wifi_ip=$(nmcli device show wlx086a0a976811 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [ -n "$wifi_ip" ]; then
    wlx_status=" ^c$color_good^ wifipi $wifi_ip^d^ $separator"
fi

# vpn

vpn_status=""
color=$color_good
if [ -e /proc/sys/net/ipv4/conf/nordlynx ]; then
    vpn_status="^c$color^  ^d^$separator"
fi

# température cpu

temp_cpu=$(cat /sys/class/thermal/thermal_zone0/temp | awk '{printf("%d",$1/1000)}')

color=$color_gray
if [ $temp_cpu -ge 70 ]; then
    color=$color_degraded
    if [ $temp_cpu -ge 80 ]; then
        color=$color_bad
    fi
fi

temp_status="^c$color^ $temp_cpu°C^d^"

# charge cpu

cpu_load=$(top -bn1 -p1 | awk '/Cpu/ {printf("%02d", $2+$4)}')
color=$color_gray
if [ $cpu_load -ge 50 ]; then
    color=$color_degraded
    if [ $cpu_load -ge 75 ]; then
        color=$color_bad
    fi
fi
cpu_status="^c$color^ $cpu_load%^d^"

# mémoire utilisée

mem_used=$(free -m | awk '/Mem/{print $3}')
color=$color_gray
if [ $mem_used -ge 500 ]; then
    color=$color_degraded
    if [ $mem_used -ge 2000 ]; then
        color=$color_bad
    fi
fi
mem_status="^c$color^ $mem_used Mb^d^"

# espace disque utilisé

disk_used=$(df | awk '/root/ {print $5}' | cut -d% -f 1)
color=$color_gray
if [ $disk_used -ge 70 ]; then
    color=$color_degraded
    if [ $disk_used -ge 85 ]; then
        color=$color_bad
    fi
fi
disk_status="^c$color^ $disk_used%^d^"

# date et heure

color=$color_gray
late_status="$(date +"%H")"
if [ $late_status -le 6 ]; then
    color=$color_bad
fi
if [ $late_status -ge 22 ]; then
    color=$color_degraded
fi
date_status="$(date +" %Y-%m-%d ^c$color^ %H:%M:%S^d^")"

# sortie

echo "$keyboard_status$eth_status$wifi_status$wlx_status$vpn_status $temp_status $separator $cpu_status $separator $mem_status $separator $disk_status $separator $date_status "
