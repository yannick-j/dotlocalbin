#!/bin/sh
# monitor wifi signal strength

link=$(cat /proc/net/wireless | awk 'NR==3 {print $3}' | head --bytes=-2)

quality="good"
if [ $link -lt 66 ]; then
    quality="degraded"
    if [ $link -lt 33 ]; then
        quality="bad"
    fi
fi

case $quality in
    good)
        color="#98971a";;
    degraded)
        color="#d65d0e";;
    bad)
        color="#cc241d";;
esac

echo "^c$color^ $link%"
