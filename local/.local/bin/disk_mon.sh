#!/bin/sh
# monitor free disk space
df -h | awk 'NR==4 {print $5}'
