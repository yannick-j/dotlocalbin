#!/bin/sh
# monitor available memory
free -h | awk 'NR==2 {print $3}'
