#!/bin/sh
# monitor cpu load in %
ps -A -o pcpu | tail -n+2 | paste -sd+ | bc
